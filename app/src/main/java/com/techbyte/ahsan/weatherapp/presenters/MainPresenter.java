package com.techbyte.ahsan.weatherapp.presenters;

/**
 * Created by Ahsan on 11/9/2016.
 */

public class MainPresenter implements MainPresenterContract {

    MainPresenterContract.ViewContract mainView;

    public MainPresenter(MainPresenterContract.ViewContract contract){
        mainView = contract;
        mainView.showSearchView();
    }

    @Override
    public void onSearchSuccessful(String cityName) {
        mainView.showResultView(cityName);
    }
}
