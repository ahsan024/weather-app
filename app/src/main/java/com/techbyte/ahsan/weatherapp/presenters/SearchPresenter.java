package com.techbyte.ahsan.weatherapp.presenters;

import android.animation.ObjectAnimator;
import android.location.Location;

import com.techbyte.ahsan.weatherapp.services.CitySearchService;
import com.techbyte.ahsan.weatherapp.services.CitySearchServiceContract;
import com.techbyte.ahsan.weatherapp.services.GpsService;
import com.techbyte.ahsan.weatherapp.services.GpsServiceContract;
import com.techbyte.ahsan.weatherapp.services.WeatherService;
import com.techbyte.ahsan.weatherapp.services.WeatherServiceContract;
import com.techbyte.ahsan.weatherapp.views.SearchViewContract;

import net.aksingh.owmjapis.CurrentWeather;

/**
 * Created by Ahsan on 11/9/2016.
 */

public class SearchPresenter implements SearchPresenterContract, CitySearchServiceContract.CityServiceListener {
    private SearchViewContract searchView;
    private GpsServiceContract gpsService;
    private CitySearchServiceContract citySearchService;
    private WeatherServiceContract weatherService;
    private String cityName;
    private OnSuccessfulWeatherUpdateListener searchResultListener;

    public SearchPresenter(SearchViewContract contract){
        searchView = contract;
    }

    @Override
    public void gpsClicked() {
        if(gpsService == null){
            gpsService = new GpsService();
        }
    }

    @Override
    public void citySearchClicked(String value) {
        citySearchService = new CitySearchService();
        citySearchService.searchForCity(value, this);
    }

    @Override
    public void locationFound(Location location) {
        if(location == null){
            searchView.showErrorMessage("Unable to find location");
        }
    }

    @Override
    public String getSearchResult() {
        return cityName;
    }

    @Override
    public void setListener(Object listener) {
        try {
            searchResultListener = (OnSuccessfulWeatherUpdateListener) listener;
        }catch (Exception e){
            e.printStackTrace();
            searchView.showErrorMessage("MainActivity doesn't implement the listener class");
        }
    }

    private void setCityName(String name){
        this.cityName = name;
    }

    @Override
    public void cityFound(String cityName) {
        if(cityName != null && !cityName.isEmpty()){
            searchResultListener.onDataRecieved(cityName);
        }else {
            searchView.showErrorMessage("City not found");
        }
    }
}
