package com.techbyte.ahsan.weatherapp.views;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.techbyte.ahsan.weatherapp.R;
import com.techbyte.ahsan.weatherapp.presenters.ResultPresenter;
import com.techbyte.ahsan.weatherapp.presenters.ResultPresenterContract;
import com.techbyte.ahsan.weatherapp.presenters.SearchPresenterContract;

import net.aksingh.owmjapis.CurrentWeather;

/**
 * Created by Ahsan on 11/9/2016.
 */

public class WeatherView extends Fragment implements WeatherViewContract {

    private final String TAG = WeatherView.class.getCanonicalName();
    private TextView mTextView;
    private ResultPresenterContract resultPresenter;
    private static String bundleKey = "city";

    public static WeatherView newInstance(String value) {
        WeatherView myFragment = new WeatherView();

        Bundle args = new Bundle();
        args.putString(bundleKey, value);
        myFragment.setArguments(args);
        return myFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_weather, container, false);

        mTextView = (TextView) mView.findViewById(R.id.weather_text);
        String cityName = getArguments().getString(bundleKey);
        mTextView.setText(cityName);

        return mView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        resultPresenter = new ResultPresenter(this);
    }

    @Override
    public void displayCurrentWeather(CurrentWeather currentWeather) {
        if(mTextView!=null && currentWeather!= null){
            mTextView.setText(currentWeather.getRawResponse());
        }else {
            showErrorMessage("Error Displaying message");
        }
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }
}
