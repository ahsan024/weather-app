package com.techbyte.ahsan.weatherapp.views;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.techbyte.ahsan.weatherapp.R;
import com.techbyte.ahsan.weatherapp.Utils;
import com.techbyte.ahsan.weatherapp.presenters.SearchPresenter;
import com.techbyte.ahsan.weatherapp.presenters.SearchPresenterContract;

import net.aksingh.owmjapis.CurrentWeather;

/**
 * Created by Ahsan on 11/8/2016.
 */

public class SearchView extends Fragment implements SearchViewContract, View.OnClickListener {

    private SearchPresenterContract.OnSuccessfulWeatherUpdateListener listener;
    private View mView;
    private Button gpsButton;
    private Button searchButton;
    private AutoCompleteTextView textView;
    private SearchPresenterContract searchPresenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        if(mView == null){
            mView = inflater.inflate(R.layout.fragment_search, container, false);
            gpsButton = (Button)mView.findViewById(R.id.gps_button);
            gpsButton.setOnClickListener(this);
            searchButton = (Button)mView.findViewById(R.id.search_button);
            searchButton.setOnClickListener(this);
            textView = (AutoCompleteTextView)mView.findViewById(R.id.editText);
//        }
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchPresenter = new SearchPresenter(this);
        searchPresenter.setListener(getActivity());
    }

    @Override
    public void gpsClicked() {
        Utils.log(SearchView.class, "GpsClicked");
        searchPresenter.gpsClicked();
    }

    @Override
    public void citySearchClicked() {
        Utils.log(SearchView.class, "citySearchClicked");
        searchPresenter.citySearchClicked(textView.getText().toString());
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getActivity(), message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void locationFound(Location location) {

    }

//    @Override
//    public void navigateToResultsScreen(SearchPresenterContract presenter) {
//        listener.onDataRecieved(presenter);
//    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.gps_button){
            gpsClicked();
        }else if(view.getId() == R.id.search_button){
            citySearchClicked();
        }
    }
}
