package com.techbyte.ahsan.weatherapp.presenters;

import android.location.Location;

import net.aksingh.owmjapis.CurrentWeather;

/**
 * Created by Ahsan on 11/9/2016.
 */

public interface SearchPresenterContract {

    void gpsClicked();
    void citySearchClicked(String value);
    void locationFound(Location location);
    String getSearchResult();
    void setListener(Object listener);


    // Container Activity must implement this interface
    public interface OnSuccessfulWeatherUpdateListener {
        void onDataRecieved(String result);
    }
}
