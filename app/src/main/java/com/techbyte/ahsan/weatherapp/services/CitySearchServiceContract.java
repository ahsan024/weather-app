package com.techbyte.ahsan.weatherapp.services;

/**
 * Created by Ahsan on 11/9/2016.
 */
public interface CitySearchServiceContract {

    void searchForCity(String name, CitySearchServiceContract.CityServiceListener listener);

    interface CityServiceListener{

        void cityFound(String cityName);
    }
}
