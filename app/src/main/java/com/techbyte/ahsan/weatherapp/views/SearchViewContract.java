package com.techbyte.ahsan.weatherapp.views;

import android.location.Location;

import com.techbyte.ahsan.weatherapp.presenters.SearchPresenterContract;

import net.aksingh.owmjapis.CurrentWeather;

/**
 * Created by Ahsan on 11/9/2016.
 */
public interface SearchViewContract{
    void gpsClicked();
    void citySearchClicked();
    void showErrorMessage(String message);
    void locationFound(Location location);
//    void navigateToResultsScreen(SearchPresenterContract contract);
}