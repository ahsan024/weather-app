package com.techbyte.ahsan.weatherapp.services;

/**
 * Created by Ahsan on 11/9/2016.
 */

public interface GpsServiceContract {
    void searchForLocation();
}
