package com.techbyte.ahsan.weatherapp.services;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.techbyte.ahsan.weatherapp.MyAsyncTask;
import com.techbyte.ahsan.weatherapp.Utils;

import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.OpenWeatherMap;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by Ahsan on 11/9/2016.
 */

public class CitySearchService implements CitySearchServiceContract {


    @Override
    public void searchForCity(String name, CitySearchServiceContract.CityServiceListener listener) {

        /*
        OpenWeatherMap openWeatherMap = new OpenWeatherMap(Utils.API_KEY);

        CurrentWeather currentWeather;
        try {
            currentWeather = openWeatherMap.currentWeatherByCityName(name);
            if(currentWeather.getResponseCode() == 200){
                listener.cityFound(currentWeather.getCityName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        */


        MyAsyncTask myAsyncTask = new MyAsyncTask();
        myAsyncTask.setCallbackClass(listener);
        myAsyncTask.execute(name);

    }

}
