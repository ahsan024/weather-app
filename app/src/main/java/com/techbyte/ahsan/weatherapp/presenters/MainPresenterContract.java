package com.techbyte.ahsan.weatherapp.presenters;

/**
 * Created by Ahsan on 11/9/2016.
 */
public interface MainPresenterContract {

//    void onSearchSuccessful(SearchPresenterContract contract);
    void onSearchSuccessful(String result);

    interface ViewContract {
        void showSearchView();
        void showResultView(String cityName);
    }
}
