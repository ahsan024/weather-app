package com.techbyte.ahsan.weatherapp.services;

import android.graphics.Path;

import com.techbyte.ahsan.weatherapp.Utils;

import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.OpenWeatherMap;

import org.json.JSONException;

import java.io.IOException;

/**
 * Created by Ahsan on 11/9/2016.
 */

public class WeatherService implements WeatherServiceContract {

    @Override
    public CurrentWeather getCurrentWeather(String city) {
        OpenWeatherMap openWeatherMap = new OpenWeatherMap(Utils.API_KEY);
        try {
            return openWeatherMap.currentWeatherByCityName(city);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
