package com.techbyte.ahsan.weatherapp.services;

import net.aksingh.owmjapis.CurrentWeather;

/**
 * Created by Ahsan on 11/9/2016.
 */
public interface WeatherServiceContract {
    CurrentWeather getCurrentWeather(String city);
}
