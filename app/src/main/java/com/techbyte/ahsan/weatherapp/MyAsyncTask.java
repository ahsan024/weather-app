package com.techbyte.ahsan.weatherapp;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.techbyte.ahsan.weatherapp.services.CitySearchServiceContract;

import net.aksingh.owmjapis.CurrentWeather;
import net.aksingh.owmjapis.OpenWeatherMap;

import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by Ahsan on 11/9/2016.
 */

public class MyAsyncTask extends AsyncTask<String, Integer , String> {

    CitySearchServiceContract.CityServiceListener callbackClass;
    String result = null;

    public void setCallbackClass(CitySearchServiceContract.CityServiceListener cb){
        callbackClass = cb;
    }

    @Override
    protected String doInBackground(String... name) {
        OpenWeatherMap openWeatherMap = new OpenWeatherMap(Utils.API_KEY);
        result = null;
        CurrentWeather currentWeather;
        try {
            currentWeather = openWeatherMap.currentWeatherByCityName(name[0]);
            if(currentWeather.getResponseCode() == 200){
                result = currentWeather.getCityName();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }
//
//    private String readStream(InputStream in) throws IOException {
//        StringBuilder result = new StringBuilder();
//        String line;
//        BufferedReader reader = new BufferedReader(new InputStreamReader(in,"utf8"));
//        while((line = reader.readLine()) != null) {
//            result.append(line);
//        }
//        System.out.println(result.toString());
//        return result.toString();
//    }

    @Override
    protected void onPostExecute(String res) {
        super.onPostExecute(res);
        Log.d(MyAsyncTask.class.getCanonicalName(), "onPostExecute " + res);
        callbackClass.cityFound(res);
    }
}
