package com.techbyte.ahsan.weatherapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;

import com.techbyte.ahsan.weatherapp.presenters.MainPresenter;
import com.techbyte.ahsan.weatherapp.presenters.MainPresenterContract;
import com.techbyte.ahsan.weatherapp.presenters.SearchPresenterContract;
import com.techbyte.ahsan.weatherapp.views.SearchView;
import com.techbyte.ahsan.weatherapp.views.WeatherView;

import net.aksingh.owmjapis.CurrentWeather;

public class MainActivity extends FragmentActivity implements  MainPresenterContract.ViewContract,SearchPresenterContract.OnSuccessfulWeatherUpdateListener {

    MainPresenterContract mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainPresenter = new MainPresenter(this);
    }

    @Override
    public void onDataRecieved(String result) {
        mainPresenter.onSearchSuccessful(result);
    }

    private void switchToResultView(SearchPresenterContract contract){

    }

    @Override
    public void showSearchView() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.fragment_space, new SearchView());
        transaction.commit();
    }

    @Override
    public void showResultView(String arg) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        Fragment fragment = WeatherView.newInstance(arg);
        transaction.replace(R.id.fragment_space, fragment);
        transaction.commit();
    }
}
