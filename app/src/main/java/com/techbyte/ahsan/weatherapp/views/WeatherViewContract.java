package com.techbyte.ahsan.weatherapp.views;

import com.techbyte.ahsan.weatherapp.presenters.SearchPresenterContract;

import net.aksingh.owmjapis.CurrentWeather;

/**
 * Created by Ahsan on 11/9/2016.
 */

public interface WeatherViewContract {
    void displayCurrentWeather(CurrentWeather currentWeather);
    void showErrorMessage(String message);
}
