package com.techbyte.ahsan.weatherapp.presenters;

import com.techbyte.ahsan.weatherapp.views.WeatherView;
import com.techbyte.ahsan.weatherapp.views.WeatherViewContract;

/**
 * Created by Ahsan on 11/9/2016.
 */

public class ResultPresenter implements ResultPresenterContract {

    private final WeatherViewContract mWeatherView;

    public ResultPresenter(WeatherViewContract weatherView) {
        mWeatherView = weatherView;
    }
}
